#!/bin/bash
podman run -it --rm \
    -v "$HOME"/.cache:/root/.cache:z \
    -v "$PWD":/wd:z \
    --workdir /wd \
    core.example.com:5000/appstream-lab/fedmod-container "$@"
